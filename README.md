<img src="MCRI-logo.png" align="right" />

# MCRI - Job Console

From time to time users of HPC systems would like to look at the output of their currently running job.
To make life easy you can use jobConsole and errorConsole to attache to the cotents of the .o or .e that are
being stored on the processing node

## Getting Started

### Prerequisites

This script assumes that you are using SSH keys and allow users to SSH to compute resources 

### Installing

```cd /path/to/src
git clone https://gitlab.com/australiaHPC/Torque/jobConsoles.git
ln -s /path/to/src/jobConsole/jobConsole.sh /usr/local/bin/jobConsoles
ln -s /path/to/src/jobConsole/errorConsole.sh /usr/local/bin/errorConsoles```

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Versioning

Current Version: 1.0

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/australiaHPC/Melbourne/jobConsole/tags). 

## Authors

* **Nick Evans** - *Initial work* - https://gitlab.com/nick.c.evans

See also the list of [contributors](https://gitlab.com/australiaHPC/Melbourne/jobConsole/graphs/master) who participated in this project.
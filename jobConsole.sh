#!/bin/bash

# jobConsole
#
# connect to and output live the contents of the .o file
# for a running PBS job
#
# Written by Nick Evans, nick.evans@mcri.edu.au
# Copyright (c) 2017 Murdoch Children's Research Institute (MCRI)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


JOBNO=$1
if [[ $JOBNO ]]
then
  COMP=`qstat -f $JOBNO | grep exec_host | sed 's/ //g' | awk -F= '{print $2}' | awk -F/ '{print $1}'`
  JOBNO=`echo $JOBNO | sed 's/\]//g' | sed 's/\[/-/g'`
  ssh $COMP tail -n 100 -f /usr/spool/PBS/spool/$JOBNO.OU
else
  echo "USAGE: $0 <jobid>"
fi